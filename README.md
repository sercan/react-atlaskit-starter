## Sercan's React Atlaskit Starter
 
This is a basic app which includes some Atlaskit components, built on top of create-react-app.

#### Install 
Please use yarn

`yarn`

#### Build

`yarn build`

### Demo

http://react-atlaskit-starter.surge.sh
