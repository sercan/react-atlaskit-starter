module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    jest: true,
    browser: true,
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            modules: ['node_modules', 'src'],
            extensions: ['.js', '.json', '.jsx'],
          },
        },
      },
    },
  },
  rules: {
    'react/jsx-filename-extension': [
      2, { extensions: ['.js'] },
    ],
    'react/prefer-stateless-function': [
      2, { 'ignorePureComponents': true }
    ],
    'import/prefer-default-export': 0,
    'react/jsx-no-bind': [2, {
      'allowArrowFunctions': false,
      'allowBind': false,
    }],
    'react/jsx-handler-names': 2,
    'jsx-a11y/anchor-is-valid': [ 2, {
      'components': [ 'Link' ],
      'specialLink': [ 'to' ],
    }],
    'object-curly-newline': 0,
    "jsx-a11y/label-has-for": [ 2, {
      "required": {
        "every": [ "id" ],
      },
    }]
  },
};
