import React, { Component } from 'react';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

export default class CodeTestPage extends Component {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>Code Challenge</PageTitle>
      </ContentWrapper>
    );
  }
}
