import React from 'react';
import MainSection from '../components/MainSection';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

const HomePage = () => (
  <ContentWrapper>
    <PageTitle>Welcome</PageTitle>
    <MainSection />
  </ContentWrapper>
);

export default HomePage;
