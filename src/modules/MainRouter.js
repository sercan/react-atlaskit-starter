import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import HomePage from '../pages/HomePage';
import CodeTestPage from '../pages/CodeTestPage';

export default class MainRouter extends Component {
  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 304,
      },
    };
  }

  getChildContext() {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,


    });
  }

  render() {
    return (
      <Router>
        <App onNavResize={this.onNavResize}>
          <Route exact path="/" component={HomePage} />
          <Route path="/code" component={CodeTestPage} />
        </App>
      </Router>
    );
  }
}

MainRouter.childContextTypes = {
  navOpenState: PropTypes.object,
};
