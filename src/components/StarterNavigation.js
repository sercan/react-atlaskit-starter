import PropTypes from 'prop-types';
import React from 'react';
import { NavLink } from 'react-router-dom';
import Nav, {
  AkNavigationItem,
} from '@atlaskit/navigation';
import DashboardIcon from '@atlaskit/icon/glyph/dashboard';
import GearIcon from '@atlaskit/icon/glyph/settings';
import atlaskitLogo from '../images/atlaskit.png';

export default class StarterNavigation extends React.Component {
  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func,
  };

  state = {
    navLinks: [
      ['/', 'Home', DashboardIcon],
      ['/code', 'Coding Test', GearIcon],
    ],
  };

  render() {
    return (
      <Nav
        isOpen={this.context.navOpenState.isOpen}
        width={this.context.navOpenState.width}
        onResize={this.props.onNavResize}
        containerHeaderComponent={() => (
          <NavLink to="/">
            <AkNavigationItem
              icon={<img alt="atlaskit logo" src={atlaskitLogo} />}
              text="Sercan's Atlaskit"
            />
          </NavLink>
        )}
      >
        {
          this.state.navLinks.map((link) => {
            const [url, title, Icon] = link;
            return (
              <NavLink key={url} to={url}>
                <AkNavigationItem
                  icon={<Icon label={title} size="medium" this />}
                  text={title}
                  isSelected={this.context.router.route.location.pathname === url}
                />
              </NavLink>
            );
          }, this)
        }
      </Nav>
    );
  }
}
